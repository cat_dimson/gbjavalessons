package lesson14;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Лекция 14")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Lesson14Test {
    private Lesson14 lesson14;

    @BeforeAll
    public void init() {
        lesson14 = new Lesson14();
    }

    /**
     * Тест для работы с валидными данными (проверяем просто длину выходного массива)
     * */
    @DisplayName("(Задача 1) Валидный массив, содержищий 4")
    @ParameterizedTest
    @MethodSource("dataProviderTask1")
    public void getArrayAfterFourTest(Integer[] arr) {
        assertEquals(3, lesson14.getArrayAfterFour(arr).length);
    }


    /**
     * Тест для работы с невалидными данными, т.е. когда массив пуст, или когда массив не содержит цифру 4
     * */
    @DisplayName("(Задача 1) Пустой массив и массив без 4")
    @ParameterizedTest
    @MethodSource("failedDataProviderTask1")
    public void getArrayAfterFourFailedTest(Integer[] arr) {
        Throwable throwable = assertThrows(RuntimeException.class, () -> lesson14.getArrayAfterFour(arr));
        assertNotNull(throwable.getMessage());
    }

    private static Stream<Arguments> dataProviderTask1() {
        return Stream.of(
                Arguments.of((Object) new Integer[] {1,2,3,4,5,6,9}),
                Arguments.of((Object) new Integer[] {4,2,5,4,5,6,0}),
                Arguments.of((Object) new Integer[] {1,4,3,4,3,1,6}),
                Arguments.of((Object) new Integer[] {1,4,2,4,3,6,1})
        );
    }

    private static Stream<Arguments> failedDataProviderTask1() {
        return Stream.of(
                Arguments.of((Object) new Integer[] {}),
                Arguments.of((Object) new Integer[] {1,2,3,5,6})
        );
    }


    /**
     * Тест для работы с невалидными данными, т.е. когда массив пуст, не содержит 1 или 4, и когда содержит лишнюю цифру
     * */
    @DisplayName("(Задача 2) Проверка на то, что массив состоит из 1 и 4 (Не валидные данные)")
    @ParameterizedTest
    @MethodSource("failedDataProviderTask2")
    public void checkArrayOnOneAndFourFailedTest(Integer[] arr) {
        assertFalse(lesson14.checkNumbersOneAndFour(arr));
    }

    /**
     * Тест для работы с валидными данными
     * */
    @DisplayName("(Задача 2) Проверка на то, что массив состоит из 1 и 4 (Валидные данные)")
    @ParameterizedTest
    @MethodSource("dataProviderTask2")
    public void checkArrayOnOneAndFourTest(Integer[] arr) {
        assertTrue(lesson14.checkNumbersOneAndFour(arr));
    }

    private static Stream<Arguments> failedDataProviderTask2() {
        return Stream.of(
                Arguments.of((Object) new Integer[] {}),
                Arguments.of((Object) new Integer[] {1,1,1,1,1,1}),
                Arguments.of((Object) new Integer[] {4,4,4,4,4,4}),
                Arguments.of((Object) new Integer[] {4,4,4,1,1,1,4,1,2})
        );
    }

    private static Stream<Arguments> dataProviderTask2() {
        return Stream.of(
                Arguments.of((Object) new Integer[] {1,4}),
                Arguments.of((Object) new Integer[] {1,1,1,1,1,1,1,4}),
                Arguments.of((Object) new Integer[] {1,4,4,4,4,4,4,4}),
                Arguments.of((Object) new Integer[] {1,4,1,4,1,4,1,4}),
                Arguments.of((Object) new Integer[] {1,1,1,1,4,4,4,4})
        );
    }
}
