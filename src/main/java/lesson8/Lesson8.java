package lesson8;

import lesson8.barrier.AbstractBarrier;
import lesson8.barrier.RunningPath;
import lesson8.barrier.Wall;
import lesson8.entity.*;

public class Lesson8 {
    public static void main(String[] args) {
        AbstractBarrier[] barriers = getBarriers();
        Overcome[] entities = getEntities();
        beginHungerGames(barriers, entities);
    }

    private static void beginHungerGames(AbstractBarrier[] barriers, Overcome[] entities) {
        // процесс преодоления препятствий
        for (AbstractBarrier barrier : barriers) {
            String consoleTitle = barrier instanceof Wall ?
                    "----- " + ((Wall) barrier).getName() + "(" + ((Wall) barrier).getHeight() + "м) -----" :
                    "----- " + ((RunningPath) barrier).getName() + "(" + ((RunningPath) barrier).getLength() + "м) -----";
            System.out.println(consoleTitle);

            for (Overcome entity : entities) {
                if (!entity.checkActive()) continue;
                if (barrier instanceof Wall) {
                    entity.jump((Wall) barrier);
                } else if (barrier instanceof RunningPath) {
                    entity.run((RunningPath) barrier);
                }
            }
        }
    }

    private static AbstractBarrier[] getBarriers() {
        // создаем препятствия
        Wall wall1 = new Wall(1, "Лайтовый барьерчек");
        Wall wall2 = new Wall(2, "Стена у гаража");
        Wall wall3 = new Wall(4, "Нормальная такая стена возле воинской части");
        RunningPath runningPath1 = new RunningPath(100, "Спринт на 100м");
        RunningPath runningPath2 = new RunningPath(400, "Кружок на стадионе");
        RunningPath runningPath3 = new RunningPath(3000, "Трешка");
        RunningPath runningPath4 = new RunningPath(21000, "Полумарафон");
        return new AbstractBarrier[] { wall1, runningPath1, wall2, runningPath2, wall3, runningPath3, runningPath4 };
    }

    private static Overcome[] getEntities() {
        // сущности, которые будут преодолевать препятствия
        Cat cat = new Cat("Барсик", 10, 200);
        Cat cat2 = new Cat("Бася", 20, 350);
        Human human1 = new Human("Вася", 30, 4000);
        Human human2 = new Human("Усейн Болт", 80, 10000);
        Robot robot1 = new Robot("Валли", 200, 15000);
        Robot robot2 = new Robot("Терминатор", 500, 25000);
        return new Overcome[] { cat, cat2, human1, human2, robot1, robot2 };
    }
}
