package lesson8.barrier;

public abstract class AbstractJumpBarrier extends AbstractBarrier {

    private final int height;
    private final String name;

    public AbstractJumpBarrier(int height, String name) {
        this.height = height;
        this.name = name;
    }

    public int getHeight() {
        return height;
    }

    public String getName() {
        return name;
    }
}
