package lesson8.barrier;

public abstract class AbstractRunningBarrier extends AbstractBarrier {

    int length;
    String name;

    public AbstractRunningBarrier(int length, String name) {
        this.length = length;
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public String getName() {
        return name;
    }
}
