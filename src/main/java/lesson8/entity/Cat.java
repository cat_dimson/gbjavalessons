package lesson8.entity;

import lesson8.barrier.AbstractJumpBarrier;
import lesson8.barrier.AbstractRunningBarrier;

public class Cat extends AbstractEntity implements Overcome {

    public Cat(String name, int jumpLimit, int runLimit) {
        super(name, jumpLimit, runLimit);
    }

    @Override
    public void run(AbstractRunningBarrier runningBarrier) {
        if (runLimit > runningBarrier.getLength()) {
            runLimit -= runningBarrier.getLength();
            System.out.printf("Кот %s пробежал %sм. Осталось сил на %sм.\n", name, runningBarrier.getLength(), runLimit);
        } else if (runLimit == runningBarrier.getLength()) {
            runLimit = 0;
            isActive = false;
            System.out.printf("! Кот %s пробежал %sм. Сил больше не осталось.\n", name, runningBarrier.getLength());
        } else {
            isActive = false;
            System.out.printf("! Коту %s не удалось преодолеть дистанцию %sм. Он выбывает.\n", name, runningBarrier.getLength());
        }
    }

    @Override
    public void jump(AbstractJumpBarrier jumpBarrier) {
        if (jumpLimit > jumpBarrier.getHeight()) {
            jumpLimit -= jumpBarrier.getHeight();
            System.out.printf("Кот %s преодолел препятствие %sм. Осталось сил на %sм.\n", name, jumpBarrier.getHeight(), jumpLimit);
        } else if (jumpLimit == jumpBarrier.getHeight()) {
            jumpLimit = 0;
            isActive = false;
            System.out.printf("! Кот %s преодолел препятствие %sм. Сил больше не осталось.\n", name, jumpBarrier.getHeight());
        } else {
            isActive = false;
            System.out.printf("! Коту %s не удалось преодолеть препятствие %sм. Он выбывает.\n", name, jumpBarrier.getHeight());
        }
    }

    @Override
    public boolean checkActive() {
        if (!isActive) {
            System.out.printf("! Кот %s больше не участвует\n", name);
        }
        return isActive;
    }
}
