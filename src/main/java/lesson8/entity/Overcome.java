package lesson8.entity;

import lesson8.barrier.AbstractJumpBarrier;
import lesson8.barrier.AbstractRunningBarrier;

public interface Overcome {

    void run(AbstractRunningBarrier runningBarrier);

    void jump(AbstractJumpBarrier jumpBarrier);

    boolean checkActive();

}
