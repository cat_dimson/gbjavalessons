package lesson8.entity;

public abstract class AbstractEntity {

    boolean isActive;
    int jumpLimit;
    int runLimit;
    String name;

    public AbstractEntity(String name, int jumpLimit, int runLimit) {
        this.name = name;
        this.jumpLimit = jumpLimit;
        this.runLimit = runLimit;
        this.isActive = true;
    }

}
