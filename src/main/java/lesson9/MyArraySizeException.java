package lesson9;

public class MyArraySizeException extends Exception {
    private int errorSize;

    public MyArraySizeException(String message, int errorSize) {
        super(message);
        this.errorSize = errorSize;
    }

    @Override
    public String toString() {
        return getMessage() + ". Ожидается - 4, а фактический размер - " + errorSize;
    }
}
