package lesson9;

public class MyArrayDataException extends NumberFormatException {

    public MyArrayDataException(String message) {
        super(message);
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
