package lesson9;

import java.util.Arrays;

public class Lesson9 {
    public static void main(String[] args) {
        String[][] arr = getArray();
        runTask(arr);
    }

    /**
     * В этом методе запускаются все основные методы и производится обрабокта исключений
     * */
    private static void runTask(String[][] arr) {
        try {
            checkArraySize(arr);
            System.out.println("Сумма элементов: " + sumArrayNums(arr));
        } catch (MyArraySizeException | MyArrayDataException e) {
            System.out.println(e);
        }
    }

    /**
     * Проверка размеров массивов. Если какой-то размеров отличается от 4, то метод выбрасывает исключение
     * MyArraySizeException
     * */
    private static void checkArraySize(String[][] arr) throws MyArraySizeException {
        // проверка размера внешнего массива
        if (arr.length != 4) {
            throw new MyArraySizeException("Ошибка размера внешнего массива", arr.length);
        }
        // проверка размеров внутренних массивов
        for (String[] innerArr : arr) {
            if (innerArr.length != 4) {
                throw new MyArraySizeException("Ошибка размера вложенного массива", innerArr.length);
            }
        }
    }

    /**
     * Метод суммирует все числа массива. Если это не возможно, то метод выбрасывает исключение
     * MyArrayDataException
     * */
    private static int sumArrayNums(String[][] arr) {
        // суммируем элементы
        try {
            return Arrays.stream(arr)
                    .flatMap(Arrays::stream)
                    .map(Integer::parseInt)
                    .reduce(0, (acc, element) -> acc + element);
        } catch (NumberFormatException e) {
            throw new MyArrayDataException("Ошибка приведения типа String -> int");
        }
    }

    /**
     * Создание матрицы
     * */
    private static String[][] getArray() {
        return new String[][] {
            new String[] {"1", "2", "3", "4"},
            new String[] {"1", "2", "3", "4"},
            new String[] {"1", "2", "3", "4"},
            new String[] {"1", "2", "3", "4"}
        };
    }
}
