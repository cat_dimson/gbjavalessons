package lesson1;

import java.time.LocalDateTime;
import java.util.Scanner;

public class Lesson1 {
    public static void main(String[] args) {
        byte byteNum = 120;
        short shortNum = 15000;
        int intNum = 2_000_000;
        long longNum = 15L;
        
        float floatNum = 2.0f;
        double doubleNum = 10.0;
        
        boolean boolVal = true; // либо false
        char charVal = 'a';
        
        String str = "Hello, GB!";
        
    
        System.out.println(getExpression(3.0f, 1.0f, 18.0f, 2.0f));
        System.out.println(isFromDiapason(10, 11));
        System.out.println(isPositiveOrZero(1));
        System.out.println(isNegative(-1));

        System.out.println("Введите имя:");
        sayGreeting(new Scanner(System.in).next());

        checkLeapYear();
    }

    /**
     * Написать метод вычисляющий выражение a * (b + (c / d)) и возвращающий результат,
     * где a, b, c, d – аргументы этого метода, имеющие тип float.
     * */
    public static float getExpression(float a, float b, float c, float d) {
        return a * (b + (c / d));
    }

    /**
     * Написать метод, принимающий на вход два целых числа и проверяющий, что их сумма лежит
     * в пределах от 10 до 20 (включительно), если да – вернуть true, в противном случае – false..
     * */
    public static boolean isFromDiapason(int num1, int num2) {
        int numSum = num1 + num2;
        return 10 <= numSum && numSum <= 20;
    }

    /**
     * Написать метод, которому в качестве параметра передается целое число, метод должен
     * напечатать в консоль, положительное ли число передали или отрицательное. Замечание: ноль
     * считаем положительным числом.
     * */
    public static boolean isPositiveOrZero(int num) {
        return num >= 0;
    }

    /**
     * Написать метод, которому в качестве параметра передается целое число. Метод должен
     * вернуть true, если число отрицательное, и вернуть false если положительное.
     * */
    public static boolean isNegative(int num) {
        if (num < 0) {
            return true;
        } else if (num > 0) {
            return false;
        } else {
            // в условии задачи про это не сказано, по этому пускай будет false
            return false;
        }
    }

    /**
     * Написать метод, которому в качестве параметра передается строка, обозначающая имя.
     * Метод должен вывести в консоль сообщение «Привет, указанное_имя!».
     * */
    public static void sayGreeting(String name) {
        System.out.println("Привет, " + name + "!");
    }

    /**
     * Написать метод, который определяет, является ли год високосным, и выводит сообщение в
     * консоль. Каждый 4-й год является високосным, кроме каждого 100-го, при этом каждый 400-й –
     * високосный
     * */
    public static void checkLeapYear() {
        LocalDateTime now = LocalDateTime.now();
        int currentYear = now.getYear();
        if (currentYear % 100 == 0) {
            if (currentYear % 400 == 0) {
                System.out.println("Год високосный");
            } else {
                System.out.println("Год не високосный");
            }
        } else {
            if (currentYear % 4 == 0) {
                System.out.println("Год високосный");
            } else {
                System.out.println("Год не високосный");
            }
        }
    }
}
