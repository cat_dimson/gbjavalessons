package lesson2;

import java.util.Arrays;

public class Lesson2 {
    public static void main(String[] args) {
        int[] invArray = {1, 1, 0, 1, 0, 1, 1, 0, 0};
        inversionArrayValues(invArray);
        System.out.println(Arrays.toString(invArray));

        int[] intVoidArray = new int[8];
        fillArray(intVoidArray);
        System.out.println(Arrays.toString(intVoidArray));

        int[] arr = {1, 5, 3, 2, 11, 4, 5, 2, 4, 8, 9, 1};
        multiplyOn2(arr);
        System.out.println(Arrays.toString(arr));

        int[][] squareArray = {
                {2, 3, 4},
                {3, 4, 5},
                {4, 5, 6}
        };
        setDiagonalElements1(squareArray);
        for (int[] innerArray: squareArray) {
            System.out.println(Arrays.toString(innerArray));
        }

        int[] arr2 = {3, 6, 2, 9, 1, 0, 6, 3, 22, 3, 3, 5};
        searchAndPrintMinAndMaxValues(arr2);

        int[] arr3 = {2, 2, 2, 1, 2, 2, 10, 1};
        System.out.println("Есть ли место баланса в массиве: " + checkBalance(arr3));

        int[] arr4 = {1, 2, 3, 4, 5, 6};
        System.out.println("Изначальный массив: " + Arrays.toString(arr4));
        shiftElementsOfArray(arr4, 2);
        System.out.println("Массив после сдвига: " + Arrays.toString(arr4));
    }

    /**
     * Задать целочисленный массив, состоящий из элементов 0 и 1. Например: [ 1, 1, 0, 0, 1, 0, 1, 1,
     * 0, 0 ]. С помощью цикла и условия заменить 0 на 1, 1 на 0;
     * */
    public static void inversionArrayValues(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                nums[i] = 0;
            } else {
                nums[i] = 1;
            }
        }
    }

    /**
     * Задать пустой целочисленный массив размером 8. С помощью цикла заполнить его
     * значениями 0 3 6 9 12 15 18 21;
     * */
    public static void fillArray(int[] voidArray) {
        for (int i = 0; i < voidArray.length; i++) {
            voidArray[i] = i * 3;
        }
    }

    /**
     * Задать массив [ 1, 5, 3, 2, 11, 4, 5, 2, 4, 8, 9, 1 ] пройти по нему циклом, и числа меньшие 6
     * умножить на 2;
     * */
    public static void multiplyOn2(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 6) {
                array[i] *= 2;
            }
        }
    }

    /**
     * Создать квадратный двумерный целочисленный массив (количество строк и столбцов
     * одинаковое), и с помощью цикла(-ов) заполнить его диагональные элементы единицами;
     * */
    public static void setDiagonalElements1(int[][] squareArray) {
        for (int i = 0; i < squareArray.length; i++) {
            squareArray[i][i] = 1;
        }
    }

    /**
     * Задать одномерный массив и найти в нем минимальный и максимальный элементы (без
     * помощи интернета);
     * */
    public static void searchAndPrintMinAndMaxValues(int[] array) {
        int min = array[0], max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
            if (max < array[i]) {
                max = array[i];
            }
        }
        System.out.println("min = " + min);
        System.out.println("max = " + max);
    }

    /**
     * Написать метод, в который передается не пустой одномерный целочисленный массив,
     * метод должен вернуть true, если в массиве есть место, в котором сумма левой и правой части
     * массива равны. Примеры: checkBalance([2, 2, 2, 1, 2, 2, || 10, 1]) → true, checkBalance([1, 1, 1, ||
     * 2, 1]) → true, граница показана символами ||, эти символы в массив не входят.
     * */
    public static boolean checkBalance(int[] array) {
        int leftSum = 0;
        int rightSum = 0;
        if (array.length == 0 || array.length == 1) {
            return false;
        }
        for (int i : array) {
            rightSum += i;
        }
        for (int i : array) {
            if (leftSum > rightSum) {
                return false;
            } else {
                leftSum += i;
                rightSum -= i;
                if (leftSum == rightSum) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Написать метод, которому на вход подается одномерный массив и число n (может быть
     * положительным, или отрицательным), при этом метод должен сместить все элементы массива
     * на n позиций. Элементы смещаются циклично. Для усложнения задачи нельзя пользоваться
     * вспомогательными массивами. Примеры: [ 1, 2, 3 ] при n = 1 (на один вправо) -> [ 3, 1, 2 ]; [ 3, 5,
     * 6, 1] при n = -2 (на два влево) -> [ 6, 1, 3, 5 ]. При каком n в какую сторону сдвиг можете
     * выбирать сами.
     * */
    public static void shiftElementsOfArray(int[] array, int shift) {
        int borderNum = shift > 0 ? array[array.length - 1] : array[0];
        if (shift > 0) {
            while (shift != 0) {
                for (int i = array.length - 1; i > 0; i--) {
                    array[i] = array[i - 1];
                }
                array[0] = borderNum;
                borderNum = array[array.length - 1];
                shift--;
            }
        } else {
            while (shift != 0) {
                for (int i = 1; i < array.length; i++) {
                    array[i - 1] = array[i];
                }
                array[array.length - 1] = borderNum;
                borderNum = array[0];
                shift++;
            }
        }
    }
}
