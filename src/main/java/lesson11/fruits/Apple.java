package lesson11.fruits;

public class Apple extends Fruit {
    public Apple(String name) {
        super(name);
        this.weight = 1.0f;
    }
}
