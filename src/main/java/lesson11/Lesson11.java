package lesson11;

import lesson11.box.Box;
import lesson11.fruits.Apple;
import lesson11.fruits.Orange;

import javax.sound.midi.Soundbank;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Lesson11 {
    public static void main(String[] args) throws CloneNotSupportedException {
        // Задание 1
        String[] strArray = new String[]{"1", "2", "3", "4", "5", "6"};
        System.out.println("--- Задание 1 ---");
        System.out.println("Массив до: " + Arrays.toString(strArray));
        replaceElementsOfArray(strArray, 0, 1);
        System.out.println("Массив после: " + Arrays.toString(strArray));

        // Задание 2
        System.out.println("--- Задание 2 ---");
        ArrayList<?> listArray = convertArrayToArrayList(strArray);
        System.out.println("Был класс: " + strArray.getClass().getName());
        System.out.println("Стал класс: " + listArray.getClass().getName());

        // Задание 3
        System.out.println("--- Задание 3 ---");
        Apple apple1 = new Apple("Яблоко 1");
        Apple apple2 = new Apple("Яблоко 2");
        Apple apple3 = new Apple("Яблоко 3");
        Orange orange1 = new Orange("Апельсин 1");
        Orange orange2 = new Orange("Апельсин 2");
        Orange orange3 = new Orange("Апельсин 3");
        Box<Apple> boxOfApple = new Box<>(
                new ArrayList<>(List.of(apple1, apple2, apple3)), "Корзина яблок 1"
        );
        Box<Orange> boxOfOrange = new Box<>(
                new ArrayList<>(List.of(orange1, orange2)), "Корзина апельсинов 1"
        );
        System.out.println("Вес коробок яблок и апельсинов равен? - " + boxOfApple.compare(boxOfOrange));
        boxOfOrange.addFruit(orange3);
        System.out.println("Вес коробок яблок и апельсинов после добавления апельсина? - " + boxOfApple.compare(boxOfOrange));

        // Из задания 3 проверим работу перекладывания фруктов из корзины в корзину
        System.out.println("--- Задание 3. Экспериментируем с перекладываением ---");
        boxOfApple.show();
        boxOfOrange.show();

        Apple apple4 = new Apple("Яблоко 4");
        Apple apple5 = new Apple("Яблоко 5");
        Apple apple6 = new Apple("Яблоко 6");
        Box<Apple> boxOfApple2 = new Box<>(
                new ArrayList<>(List.of(apple4, apple5, apple6)), "Корзина яблок 2"
        );
        boxOfApple2.show();

        System.out.println("--- Перекладываем яблоки ---");
        boxOfApple.addFruitsFromBox(boxOfApple2, 2);
        boxOfApple.show();
        boxOfApple2.show();
    }

    /**
     * Задание 1. Написать метод, который меняет два элемента массива местами (массив может быть любого
     * ссылочного типа);
     */
    public static <T> void replaceElementsOfArray(T[] array, int index1, int index2) {
        if (index1 != index2) {
            T element = array[index1];
            array[index1] = array[index2];
            array[index2] = element;
        }
    }

    /**
     * Задание 2. Написать метод, который преобразует массив в ArrayList;
     */
    public static <T> ArrayList<T> convertArrayToArrayList(T[] array) {
        return new ArrayList<>(List.of(array));
    }
}