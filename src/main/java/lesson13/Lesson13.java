package lesson13;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Lesson13 {
    public static final int CARS_COUNT = 4;
    public static final CountDownLatch waitCars = new CountDownLatch(CARS_COUNT);
    public static final CountDownLatch waitFinishCars = new CountDownLatch(CARS_COUNT);
    public static final CyclicBarrier prepareCars = new CyclicBarrier(CARS_COUNT);
    public static final Semaphore semaphoreTunnel = new Semaphore(CARS_COUNT/2);

    public volatile static boolean isWinner = false;
    public volatile static boolean choosingWinnerFlag = false;
    public static final CountDownLatch choosingWinnerCDL = new CountDownLatch(1);

    public static void main(String[] args) throws InterruptedException {
        System.out.println("ВАЖНОЕ ОБЪЯВЛЕНИЕ >>> Подготовка!!!");
        Race race = new Race(new Road(60), new Tunnel(), new Road(40));

        Car[] cars = new Car[CARS_COUNT];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = new Car(race, 20 + (int) (Math.random() * 10));
        }

        for (int i = 0; i < cars.length; i++) {
            new Thread(cars[i]).start();
        }
        waitCars.await();
        System.out.println("ВАЖНОЕ ОБЪЯВЛЕНИЕ >>> Гонка началась!!!");

        waitFinishCars.await();
        System.out.println("ВАЖНОЕ ОБЪЯВЛЕНИЕ >>> Гонка закончилась!!!");
    }

    public static void printWinner(String name) {
        System.out.println(name + " WIN");
        Lesson13.isWinner = true;
    }
}
