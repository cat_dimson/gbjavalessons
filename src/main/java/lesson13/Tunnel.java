package lesson13;

public class Tunnel extends Stage {
    public Tunnel() {
        this.length = 80;
        this.description = "Тоннель " + length + " метров";
    }
    @Override
    public void go(Car c) {
        try {
            try {

                while (Lesson13.choosingWinnerFlag) {
                    Lesson13.choosingWinnerCDL.await();
                }
                System.out.println(c.getName() + " готовится к этапу(ждет): " +
                        description);

                while (Lesson13.choosingWinnerFlag) {
                    Lesson13.choosingWinnerCDL.await();
                }
                Lesson13.semaphoreTunnel.acquire();

                while (Lesson13.choosingWinnerFlag) {
                    Lesson13.choosingWinnerCDL.await();
                }
                System.out.println(c.getName() + " начал этап: " + description);

                while (Lesson13.choosingWinnerFlag) {
                    Lesson13.choosingWinnerCDL.await();
                }
                Thread.sleep(length / c.getSpeed() * 1000);

                while (Lesson13.choosingWinnerFlag) {
                    Lesson13.choosingWinnerCDL.await();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {

                while (Lesson13.choosingWinnerFlag) {
                    Lesson13.choosingWinnerCDL.await();
                }
                System.out.println(c.getName() + " закончил этап: " +
                        description);

                while (Lesson13.choosingWinnerFlag) {
                    Lesson13.choosingWinnerCDL.await();
                }
                Lesson13.semaphoreTunnel.release();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
