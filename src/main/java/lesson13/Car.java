package lesson13;

public class Car implements Runnable {
    private static int CARS_COUNT;
    private Race race;
    private int speed;
    private String name;
    public String getName() {
        return name;
    }
    public int getSpeed() {
        return speed;
    }
    public Car(Race race, int speed) {
        this.race = race;
        this.speed = speed;
        CARS_COUNT++;
        this.name = "Участник #" + CARS_COUNT;
    }
    @Override
    public void run() {
        try {
            System.out.println(this.name + " готовится");
            Thread.sleep(500 + (int)(Math.random() * 800));
            Lesson13.prepareCars.await();
            System.out.println(this.name + " готов");
            Lesson13.waitCars.countDown();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < race.getStages().size(); i++) {
            race.getStages().get(i).go(this);
        }

        if (!Lesson13.isWinner) {
            Lesson13.choosingWinnerFlag = true;
            Lesson13.printWinner(this.name);
            Lesson13.choosingWinnerCDL.countDown();
            Lesson13.choosingWinnerFlag = false;
        }

        Lesson13.waitFinishCars.countDown();
    }
}
