package lesson13;

public class Road extends Stage {
    public Road(int length) {
        this.length = length;
        this.description = "Дорога " + length + " метров";
    }
    @Override
    public void go(Car c) {
        try {
            while (Lesson13.choosingWinnerFlag) {
                Lesson13.choosingWinnerCDL.await();
            }
            System.out.println(c.getName() + " начал этап: " + description);

            while (Lesson13.choosingWinnerFlag) {
                Lesson13.choosingWinnerCDL.await();
            }
            Thread.sleep(length / c.getSpeed() * 1000);

            while (Lesson13.choosingWinnerFlag) {
                Lesson13.choosingWinnerCDL.await();
            }
            System.out.println(c.getName() + " закончил этап: " + description);

            while (Lesson13.choosingWinnerFlag) {
                Lesson13.choosingWinnerCDL.await();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
