package lesson13.examples;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorServiceDemo {

    /** Синхронный метод с проверкой на выполнение/невыполнение потока */
//    public static void main(String[] args) throws Exception {
//        ExecutorService executorService = Executors.newFixedThreadPool(2);
//        Future future = executorService.submit(new Runnable() {
//            public void run() {
//                System.out.println("Запущен поток из экзекьютор сервиса");
//                try {
//                    Thread.sleep(2000);
//                    System.out.println("Запущенный поток преостановил свою работу");
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                System.out.println("Асинхронная задача");
//            }
//        });
//        System.out.println(future.get());
//        Thread.sleep(2500);
//        System.out.println(future.get()); // вернет null если задача завершилась корректно
//        executorService.shutdown();
//    }

    /** Синхронный метод с возвращением результата */
//    public static void main(String[] args) throws Exception {
//        ExecutorService executorService = Executors.newFixedThreadPool(2);
//        Future future = executorService.submit(new Callable(){
//            public Object call() throws Exception {
//                System.out.println("Асинхронный вызов");
//                Thread.sleep(3000);
//                return "Результат из потока";
//            }
//        });
//        System.out.println("future.get() = " + future.get());
//        executorService.shutdown();
//    }

    /** Асинхронный метод */
//    public static void main(String[] args) {
//        ExecutorService executorService = Executors.newFixedThreadPool(10);
//        executorService.execute(new Runnable() {
//            public void run() {
//                try {
//                    System.out.println("Стартует поток");
//                    Thread.sleep(3000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                System.out.println("Асинхронная задача");
//            }
//        });
//
//        System.out.println("Основной поток");
//        executorService.shutdown();
//    }
    /**
     * ExecutorService нужно закрывать в конце: shutdown() и shutdownNow(). Это не прервет выполняемые в нем потоки,
     * но запретит брать ему новые задачи, т.е. остановит свободные потоки. Если не закрыть сервис, то его открытые
     * потоки не дадут закончить работу JVM.
     * shutdownNow() - закрывает потоки немедленно, даже если они выполнялись
     * */
}
