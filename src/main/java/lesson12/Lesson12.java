package lesson12;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

public class Lesson12 {
    static final int SIZE = 10_000_000;
    static final int HALF = SIZE / 2;

    public static void main(String[] args) {
        System.out.println("Start");

        Lesson12 lesson12 = new Lesson12();
        float[] arr1 = lesson12.initArray(SIZE);
        float[] arr2 = lesson12.initArray(SIZE);

        System.out.println("Время выполнения синхронного метода: " + lesson12.method1Sync(arr1));
        System.out.println("Время выполнения асинхронного метода в 2 потока: " + lesson12.method2Async(arr2));

        System.out.println("Finish");
    }

    public float[] initArray(int size) {
        float[] returnArr = new float[size];
        Arrays.fill(returnArr, 1);
        return returnArr;
    }

    private void calculation(float[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (float)(arr[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
        }
    }

    public long method1Sync(float[] arr) {
        long start = System.currentTimeMillis();
        calculation(arr);
        long end = System.currentTimeMillis();

        return end - start;
    }

    public long method2Async(float[] arr) {
        CountDownLatch cdl = new CountDownLatch(2);

        float[] arr1 = new float[HALF];
        float[] arr2 = new float[HALF];

        long start = System.currentTimeMillis();

        System.arraycopy(arr, 0, arr1, 0, HALF);
        System.arraycopy(arr, HALF, arr2, 0, HALF);

        new Thread(() -> {
            calculation(arr1);
            cdl.countDown();
        }).start();
        new Thread(() -> {
            calculation(arr2);
            cdl.countDown();
        }).start();

        try {
            cdl.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.arraycopy(arr1, 0, arr, 0, HALF);
        System.arraycopy(arr2, 0, arr, HALF, HALF);

        long end = System.currentTimeMillis();

        return end - start;
    }
}
