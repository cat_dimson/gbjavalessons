package lesson3;

import java.util.Objects;
import java.util.Scanner;

/**
 * Создать массив из слов
 * String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot", "cherry",
 * "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea", "peanut", "pear", "pepper",
 * "pineapple", "pumpkin", "potato"}.
 * При запуске программы компьютер загадывает слово, запрашивает ответ у пользователя, сравнивает его с загаданным
 * словом и сообщает, правильно ли ответил пользователь. Если слово не угадано, компьютер показывает буквы, которые
 * стоят на своих местах.
 * apple – загаданное
 * apricot - ответ игрока
 * ap############# (15 символов, чтобы пользователь не мог узнать длину слова)
 * */
public class Lesson3_2 {
    public static void main(String[] args) {
        String[] words = initWords();
        String computerWord = getComputerWord(words);
        String playerWord = getPlayerWord();
        guessWordProcess(computerWord, playerWord);
    }

    private static String[] initWords() {
        return new String[] {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot", "cherry",
                "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea", "peanut", "pear",
                "pepper", "pineapple", "pumpkin", "potato"};
    }

    private static String getComputerWord(String[] words) {
        int index = (int) (Math.random() * words.length);
        //return words[index];
        return "orange";
    }

    private static String getPlayerWord() {
        System.out.print("Введите слово: ");
        Scanner scan = new Scanner(System.in);
        return scan.nextLine();
    }

    private static void guessWordProcess(String computerWord, String playerWord) {
        Scanner scan = new Scanner(System.in);
        while (true) {
            if (Objects.equals(computerWord, playerWord)) {
                System.out.println("Правильно, вы угадали! Это слово " + playerWord);
                break;
            } else {
                System.out.print("Вы не угадали слово\n" +
                        getPartSolution(computerWord, playerWord) + "\n" +
                        "Попробуйте еще раз: ");
                playerWord = scan.nextLine();
            }
        }
    }

    private static String getPartSolution(String computerWord, String playerWord) {
        int border = Math.min(computerWord.length(), playerWord.length());
        int quantityMask = 15 - border;
        StringBuilder answer = new StringBuilder();
        for (int i = 0; i < border; i++) {
            if (computerWord.charAt(i) == playerWord.charAt(i)) {
                answer.append(computerWord.charAt(i));
            } else {
                answer.append("#");
            }
        }
        answer.append(generateMask(quantityMask));
        return answer.toString();
    }

    private static String generateMask(int n) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < n; i++) {
            s.append("#");
        }
        return s.toString();
    }
}
