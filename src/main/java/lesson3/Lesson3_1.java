package lesson3;

import java.util.Random;
import java.util.Scanner;

/**
 * Написать программу, которая загадывает случайное число от 0 до 9 и пользователю дается 3 попытки угадать это число.
 * При каждой попытке компьютер должен сообщить, больше ли указанное пользователем число, чем загаданное, или меньше.
 * После победы или проигрыша выводится запрос – «Повторить игру еще раз? 1 – да / 0 – нет»(1 – повторить, 0 – нет).
 * */
public class Lesson3_1 {
    public static void main(String[] args) {
        startGame();
    }

    private static void startGame() {
        gameProcess();
    }

    private static void gameProcess() {
        while(true) {
            int numberComputer = getNumberComputer();
            int numberPlayer = getNumberPlayer();
            int playerChoice;

            if (numberComputer == numberPlayer) {
                playerChoice = equalsVariant();
            } else {
                playerChoice = notEqualsVariant(numberPlayer, numberComputer);
            }

            if (playerChoice == 0) {
                break;
            }
        }
    }

    private static int notEqualsVariant(int numberPlayer, int numberComputer) {
        int limitMiss = 2;
        while (limitMiss != 0) {
            System.out.println("Не правильно. " + meleeOrMore(numberPlayer, numberComputer) +
                    "\nПопробуйте еще раз.");
            System.out.println("Количество попыток: " + limitMiss--);
            numberPlayer = getNumberPlayer();
            if (numberComputer == numberPlayer) {
                return equalsVariant();
            }
        }
        return fail(numberComputer);
    }

    private static int fail(int numberComputer) {
        System.out.println("Увы. Число не угадано. Загаданное число " + numberComputer + "\n" +
                "Повторить игру еще раз? 1 – да / 0 – нет");
        Scanner scan = new Scanner(System.in);
        return scan.nextInt();
    }

    private static int equalsVariant() {
        System.out.println("Угадал!\n" +
                "Повторить игру еще раз? 1 – да / 0 – нет");
        Scanner scan = new Scanner(System.in);
        return scan.nextInt();
    }

    private static int getNumberPlayer() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число от 0 до 9: ");
        int numberPlayer;
        while (true) {
            numberPlayer = scan.nextInt();
            if (0 <= numberPlayer && numberPlayer<= 9) {
                break;
            } else {
                System.out.println("Число должно быть в пределах от 0 до 9");
            }
        }
        return numberPlayer;
    }

    private static int getNumberComputer() {
        return (int) (Math.random() * 10);
    }

    private static String meleeOrMore(int numPlayer, int numComputer) {
        return numPlayer < numComputer ? "Ваше число меньше." : "Ваше число больше.";
    }
}
