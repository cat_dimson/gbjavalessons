package lesson7;

import java.util.SplittableRandom;

public class Cat {
    private final String name;
    private final int appetite;
    private boolean satiety = false;

    public Cat(String name, int appetite) {
        this.name = name;
        this.appetite = appetite;
    }

    public String getNickname() {
        return name;
    }

    public void eat(Plate plate) {
        if (appetite < plate.quantityFood() && !satiety) {
            plate.eatFood(appetite);
            this.toSatiety();
        } else {
            System.out.println("Еды слишком мало в миске. Кот " + name + " сыт не будет");
        }
    }

    public void toSatiety() {
        satiety = true;
    }

    public void toHungry() {
        satiety = false;
    }

    public boolean isSatiety() {
        return satiety;
    }
}
