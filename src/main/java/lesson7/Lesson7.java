package lesson7;

public class Lesson7 {
    public static void main(String[] args) {
        Cat[] cats = {
            new Cat("Тоша", 5),
            new Cat("Бася", 6),
            new Cat("Снежок", 8),
            new Cat("Бендер", 15)
        };
        Plate plate = new Plate(20);

        plate.info();

        System.out.println("--- Покормим котов ---");
        for (Cat cat : cats) {
            cat.eat(plate);
        }

        System.out.println("--- Проверим, сыты ли коты ---");
        for (Cat cat : cats) {
            System.out.println("Кот " + cat.getNickname() + " сыт: " + cat.isSatiety());
        }

        System.out.println("--- Поели все, на Бендера не хватило ---");
        plate.info();

        System.out.println("--- Насыпали еды в миску ---");
        plate.fillPlate(20);
        plate.info();

        System.out.println("--- Поел последний кот (Бендер) ---");
        cats[3].eat(plate);
        plate.info();
        System.out.println("Кот бендер сыт: " + cats[3].isSatiety());
    }
}
