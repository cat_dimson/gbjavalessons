package lesson7;

public class Plate {
    private int food;
    private final int FOOD_LIMIT = 20;

    public Plate(int food) {
        this.food = Math.min(food, FOOD_LIMIT);
    }

    public void info() {
        System.out.println("Остаток в миске: " + food);
    }

    public void eatFood(int n) {
        food -= n;
    }

    public int quantityFood() {
        return food;
    }

    public void fillPlate(int food) {
        this.food = Math.min(food, FOOD_LIMIT);
    }
}
