package lesson6;

public class Cat extends Animal {
    private static int count = 0;
    private int runLimit = 200;
    private final String nickname;

    public Cat(String nickname) {
        this.nickname = nickname;
        Cat.count++;
    }

    public static void printCountCats() {
        System.out.println("Создано экземпляров котов: " + Cat.count);
    }

    @Override
    public void run(int length) {
        if (runLimit - length >= 0) {
            runLimit -= length;
            System.out.println("Кот " + nickname + " пробежал: " + length + "м");
        } else {
            System.out.println("Кот " + nickname + " больше не может бежать. Его запас " + runLimit + "м");
        }
    }

    @Override
    public void swim(int length) {
        System.out.println("Коты не умеют плавать");
    }
}
