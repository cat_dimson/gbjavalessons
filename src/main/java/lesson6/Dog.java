package lesson6;

public class Dog extends Animal {
    private static int count = 0;
    private int runLimit = 500;
    private int swimLimit = 10;
    private final String nickname;

    public Dog(String nickname) {
        this.nickname = nickname;
        Dog.count++;
    }

    public static void printCountDogs() {
        System.out.println("Создано экземпляров собак: " + Dog.count);
    }

    @Override
    public void run(int length) {
        if (runLimit - length >= 0) {
            runLimit -= length;
            System.out.println("Пёс " + nickname + " пробежал: " + length + "м");
        } else {
            System.out.println("Пёс " + nickname + " больше не может бежать. Его запас " + runLimit + "м");
        }
    }

    @Override
    public void swim(int length) {
        if (swimLimit - length >= 0) {
            swimLimit -= length;
            System.out.println("Пёс " + nickname + " проплыл: " + length + "м");
        } else {
            System.out.println("Пёс " + nickname + " больше не может плыть. Его запас " + swimLimit + "м");
        }
    }
}
