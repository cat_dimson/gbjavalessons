package lesson6;

import java.util.Scanner;

public class Lesson6 {
    public static void main(String[] args) {
        workWithAnimal();
        workWithCats();
        workWithDogs();
    }

    public static void workWithAnimal() {
        Animal animal1 = new Animal();
        Animal.printCountAnimals();
        animal1.run(200);
        animal1.swim(3);
        Animal animal2 = new Animal();
        Animal.printCountAnimals();
        animal2.run(300);
        animal2.swim(5);
        System.out.println("---------------------------");
    }

    public static void workWithCats() {
        Cat cat1 = new Cat("Барсик");
        Cat.printCountCats();
        cat1.run(40);
        cat1.swim(5);
        cat1.run(90);
        cat1.run(200);
        System.out.println("---------------------------");

        Cat cat2 = new Cat("Мурзик");
        Cat.printCountCats();
        cat2.run(200);
        cat2.run(20);
        cat2.swim(1);
        System.out.println("---------------------------");

        Cat cat3 = new Cat("Бакс");
        Cat.printCountCats();
        System.out.println("---------------------------");

        Animal.printCountAnimals();
        System.out.println("---------------------------");
    }

    public static void workWithDogs() {
        Dog dog1 = new Dog("Бобик");
        Dog.printCountDogs();
        dog1.run(350);
        dog1.swim(5);
        dog1.run(190);
        dog1.swim(6);
        System.out.println("---------------------------");

        Dog dog2 = new Dog("Шарик");
        Dog.printCountDogs();
        dog2.run(200);
        dog2.run(300);
        dog2.swim(10);
        System.out.println("---------------------------");

        Dog dog3 = new Dog("Герда");
        Dog dog4 = new Dog("Дворняга");
        Dog.printCountDogs();
        System.out.println("---------------------------");

        Animal.printCountAnimals();
        System.out.println("---------------------------");
    }
}
