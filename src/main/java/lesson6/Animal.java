package lesson6;

public class Animal {
    protected static int count = 0;

    public Animal() {
        Animal.count++;
    }

    public static void printCountAnimals() {
        System.out.println("Создано экземпляров животных: " + Animal.count);
    }

    public void run(int length) {
        System.out.println("Животное пробегает " + length + "м");
    }

    public void swim(int length) {
        System.out.println("Животное плывёт " + length + "м");
    }

}
