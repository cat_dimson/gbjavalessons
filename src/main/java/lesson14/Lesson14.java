package lesson14;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Lesson14 {
    public static void main(String[] args) {

    }

    /**
     * Задание 1
     */
    public Integer[] getArrayAfterFour(Integer[] arr) {
        if (checkSizeZeroArray(arr)) {
            throw new RuntimeException("Массив пуст");
        }
        if (!checkNumberFourInArray(arr)) {
            throw new RuntimeException("Массив не содержит цифры 4");
        }

        List<Integer> sourceList = Arrays.asList(arr);
        int indexOf4 = sourceList.lastIndexOf(4);
        List<Integer> resultList = sourceList.subList(indexOf4 + 1, sourceList.size());

        return resultList.toArray(new Integer[resultList.size()]);
    }

    /**
     * Содержит ли число 4
     */
    private static boolean checkNumberFourInArray(Integer[] arr) {
        return Arrays.stream(arr).anyMatch(x -> x == 4);
    }

    /**
     * Пустой ли массив
     */
    private static boolean checkSizeZeroArray(Integer[] arr) {
        return arr.length == 0;
    }

    /**
     * Задание 2
     * Проверяем, состоит ли массив только из 1 и 4
     */
    public boolean checkNumbersOneAndFour(Integer[] arr) {
        Set<Integer> set = new HashSet<>(Arrays.asList(arr));
        return set.size() == 2 && set.contains(1) && set.contains(4);
    }
}
