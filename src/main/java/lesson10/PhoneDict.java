package lesson10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhoneDict {
    private Map<String, ArrayList<String>> phoneDict = new HashMap<>();

    public PhoneDict() {
    }

    public PhoneDict(Map<String, ArrayList<String>> phoneDict) {
        this.phoneDict.putAll(phoneDict);
    }

    public void add(String lastName, String numberPhone) {
        if (phoneDict.containsKey(lastName)) {
            if (!phoneDict.get(lastName).contains(numberPhone)) {
                phoneDict.get(lastName).add(numberPhone);
            }
        } else {
            phoneDict.put(lastName, new ArrayList<>(List.of(numberPhone)));
        }
    }

    public ArrayList<String> get(String lastName) {
        return phoneDict.get(lastName);
    }

    public void print() {
        phoneDict.forEach((key, value) -> System.out.printf("%s: %s,\n", key, value));
    }
}
