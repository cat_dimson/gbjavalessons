package lesson10;

import java.util.*;

import static java.util.Map.entry;

public class Lesson10 {
    public static void main(String[] args) {
        // задание 1
        System.out.println("---Задание 1---");
        String[] array = getArray();
        printUniqueValues(array);

        // задание 2
        System.out.println("---Задание 2---");
        PhoneDict phoneDict = new PhoneDict(
                Map.of(
                    "Иванов", new ArrayList<> (List.of("89205558877") ),
                    "Сидоров", new ArrayList<> (List.of("89105556600") ),
                    "Петров", new ArrayList<> (List.of("89105556611") )
                )
        );
        phoneDict.print();
        phoneDict.add("Иванов", "89511343788");
        phoneDict.add("Иванов", "89511343744");
        phoneDict.add("Сидоров", "003");
        phoneDict.add("Сидоров", "004");
        phoneDict.add("Петров", "004");
        phoneDict.add("Петров", "004");
        phoneDict.add("Петров", "007");
        phoneDict.add("Куприн", "009");
        System.out.println("---После добавления---");
        phoneDict.print();
        System.out.println("---Извлекаем из справочника по ключу---");
        System.out.println("Петров: " + phoneDict.get("Петров"));
        System.out.println("Сидоров: " + phoneDict.get("Сидоров"));
        System.out.println("Иванов: " + phoneDict.get("Иванов"));
        System.out.println("Куприн: " + phoneDict.get("Куприн"));
    }

    private static String[] getArray() {
        return new String[] {
                "яблоко", "апельсин", "лимон", "яблоко", "слива",
                "яблоко", "мандарин", "груша", "вишня", "банан",
                "лимон", "банан", "ананас", "персик", "персик",
                "нектарин", "персик", "банан", "лайм", "черешня"
        };
    }

    private static void printUniqueValues(String[] array) {
        Set<String> uniqueValues = new HashSet<>(List.of(array));
        System.out.println(uniqueValues);
    }
}
