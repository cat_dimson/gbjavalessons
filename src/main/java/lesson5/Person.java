package lesson5;

import java.math.BigDecimal;

public class Person {
    private String fio;
    private String position;
    private String email;
    private String phone;
    private BigDecimal salary;
    private int age;

    public Person(String fio, String position, String email, String phone, BigDecimal salary, int age) {
        this.fio = fio;
        this.position = position;
        this.email = email;
        this.phone = phone;
        this.salary = salary;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public String getInfo() {
        return "Person {" +
            "fio='" + fio + '\'' +
            ", position='" + position + '\'' +
            ", email='" + email + '\'' +
            ", phone='" + phone + '\'' +
            ", salary=" + salary +
            ", age=" + age +
            '}';
    }
}
