package lesson5;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Lesson5 {
    public static void main(String[] args) {
        Person[] persons = new Person[5];

        // person 1
        BigDecimal selery1 = new BigDecimal(35000);
        selery1 = selery1.setScale(2, RoundingMode.HALF_UP);
        persons[0] = new Person("Илларионов Дмитрий Степанович", "Младший разработчик", "dkotik@mail.ru",
                "89205058920", selery1, 28);

        // person 2
        BigDecimal selery2 = new BigDecimal(25000);
        selery2 = selery2.setScale(2, RoundingMode.HALF_UP);
        persons[1] = new Person("Иванов Иван Иванович", "Администратор", "ivashka@gmail.ru",
                "89205558899", selery2, 34);

        // person 3
        BigDecimal selery3 = new BigDecimal(55000);
        selery3 = selery3.setScale(2, RoundingMode.HALF_UP);
        persons[2] = new Person("Евгеньев Евгений Евгеньевич", "Разработчик", "evgeshka@gmail.ru",
                "89511343766", selery3, 41);

        // person 4
        BigDecimal selery4 = new BigDecimal(75000);
        selery4 = selery4.setScale(2, RoundingMode.HALF_UP);
        persons[3] = new Person("Максимов Максим Максимович", "Руководитель отдела", "maksmaks@yandex.ru",
                "89516669532", selery4, 47);

        // person 5
        BigDecimal selery5 = new BigDecimal(40000);
        selery5 = selery5.setScale(2, RoundingMode.HALF_UP);
        persons[4] = new Person("Тушакова Екатерина Валерьевна", "Руководитель проекта", "katya@yandex.ru",
                "89516669632", selery5, 30);

        for (int i = 0; i < persons.length; i++) {
            if (persons[i].getAge() > 40) {
                System.out.println("-----------------");
                System.out.println(persons[i].getInfo());
            }
        }
    }
}
