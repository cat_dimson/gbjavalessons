package lesson4;

import java.util.Random;
import java.util.Scanner;

public class Lesson4 {
    public static char[][] map;
    public static final int MAP_SIZE = 9;
    public static final int LIMIT_DOTS = 4;
    public static final char DOT_VOID = '•';
    public static final char DOT_X = 'x';
    public static final char DOT_O = 'o';
    public static Scanner sc = new Scanner(System.in);
    public static Random random = new Random();

    public static void main(String[] args) {
        initMap();
        printMap();
        while (true) {
            playerTurn();
            printMap();
            if (checkWin(DOT_X)) {
                System.out.println("Игрок победил");
                break;
            }
            if (isMapFull()) {
                System.out.println("Ничья");
                break;
            }
            computerTurn();
            printMap();
            if (checkWin(DOT_O)) {
                System.out.println("Компьютер победил");
                break;
            }
            if (isMapFull()) {
                System.out.println("Ничья");
                break;
            }
        }
        System.out.println("Конец игры");
    }

    public static void initMap() {
        map = new char[MAP_SIZE][MAP_SIZE];
        for (int i = 0; i < MAP_SIZE; i++) {
            for (int j = 0; j < MAP_SIZE; j++) {
                map[i][j] = DOT_VOID;
            }
        }
    }

    public static void printMap() {
        for (int i = 0; i <= MAP_SIZE; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        for (int i = 1; i <= MAP_SIZE; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < MAP_SIZE; j++) {
                System.out.print(map[i - 1][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void playerTurn() {
        int x, y;
        do {
            System.out.println("Введите Х и Y: ");
            x = sc.nextInt();
            y = sc.nextInt();
        } while (!isCellValid(x - 1, y - 1));
        map[y - 1][x - 1] = DOT_X;
    }

    public static boolean isCellValid(int x, int y) {
        if (y < 0 || y >= MAP_SIZE || x < 0 || x > MAP_SIZE) return false;
        if (map[y][x] == DOT_VOID) return true;
        return false;
    }

    public static void computerTurn() {
        int x, y;
        do {
            x = random.nextInt(MAP_SIZE);
            y = random.nextInt(MAP_SIZE);
        } while (!isCellValid(x, y));
        map[y][x] = DOT_O;
    }

    public static boolean isMapFull() {
        for (int i = 0; i < MAP_SIZE; i++) {
            for (int j = 0; j < MAP_SIZE; j++) {
                if (map[i][j] == DOT_VOID) return false;
            }
        }
        return true;
    }

    public static boolean checkWin(char symb) {
        // Проверка победы по горизонтали для любого размера поля и любой длины последовательности символов
        // 1. Идем по рядам
        for (int i = 0; i < MAP_SIZE; i++) {
            // 2. Запускаем цикл для проверки всех возможных сочетаний из LIMIT_DOTS символов
            outer:
            for (int j = 0; j < MAP_SIZE - LIMIT_DOTS + 1; j++) {
                // 3. Если в ряду из LIMIT_DOTS символов все символы одинаковые, то победа
                for (int k = 0; k < LIMIT_DOTS; k++) {
                    if (map[i][k + j] != symb) continue outer;
                }
                return true;
            }
        }

        // Проверка победы по вертикали для любого размера поля и любой длины последовательности символов. Всё по
        // аналогии с горизонтальной проверкой
        // 1. Идем по столбцам
        for (int i = 0; i < MAP_SIZE; i++) {
            // 2. Запускаем цикл для проверки всех возможных сочетаний из LIMIT_DOTS символов
            outer:
            for (int j = 0; j < MAP_SIZE - LIMIT_DOTS + 1; j++) {
                // 3. Если в ряду из LIMIT_DOTS символов все символы одинаковые, то победа
                for (int k = 0; k < LIMIT_DOTS; k++) {
                    if (map[k + j][i] != symb) continue outer;
                }
                return true;
            }
        }

        // Проверка Все диагональные варианты
        // 1. Идем по главной (и ||-ым ей) диагоналям, длина которых >= LIMIT_DOTS
        for (int i = MAP_SIZE - LIMIT_DOTS; i >= 0; i--) {
            // 2. Запускаем цикл для проверки всех возможных сочетаний из LIMIT_DOTS символов в текущей главной диагонали\
            for (int j = 0; j < MAP_SIZE - LIMIT_DOTS - i + 1; j++) {
                // 3. Если в ряду из LIMIT_DOTS символов по диагонали все символы одинаковые, то победа
                // слева от главной диагонали (+ главная)
                for (int k = 0; true; k++) {
                    if (map[i + k + j][k + j] != symb) break;
                    if (k == LIMIT_DOTS - 1) {
                        return true;
                    }
                }
                // справа от главной диагонали (+ главная)
                for (int k = 0; true; k++) {
                    if (map[k + j][i + k + j] != symb) break;
                    if (k == LIMIT_DOTS - 1) {
                        return true;
                    }
                }
            }
        }

        // Проверка слева от главной диагонали и справа.
        // 1. Идем по второстепенной (и ||-ым ей) диагоналям, длина которых >= LIMIT_DOTS
        for (int i = LIMIT_DOTS - 1; i < MAP_SIZE; i++) {
            // 2. Запускаем цикл для проверки всех возможных сочетаний из LIMIT_DOTS символов в текущей главной диагонали\
            for (int j = 0; j <= i + 1 - LIMIT_DOTS; j++) {
                // 3. Если в ряду из LIMIT_DOTS символов по диагонали все символы одинаковые, то победа
                // слева от второстепенной диагонали (+ главная)
                for (int k = 0; true; k++) {
                    if (map[j + k][i - j - k] != symb) break;
                    if (k == LIMIT_DOTS - 1) {
                        return true;
                    }
                }
                // справа от второстепенной диагонали (+ главная)
                for (int k = 0; true; k++) {
                    if (map[MAP_SIZE - 1 - j - k][MAP_SIZE - i - 1 + j + k] != symb) break;
                    if (k == LIMIT_DOTS - 1) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
